import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { Paginated } from '@feathersjs/feathers';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-things',
  templateUrl: './things.component.html',
  styleUrls: ['./things.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThingsComponent implements OnInit {
  things$: Observable<any[]>;
  thingForm;

  constructor(
    private data: DataService,
    private formBuilder: FormBuilder
  ) { 
    // get things from data service
    this.things$ = data.things$().pipe(
        // our data is paginated, so map to .data
        map((t: Paginated<any>) => t.data)
      );
  }
  
  onSubmit(thingData) {
    this.data.addThing( thingData.desc );
  }

  ngOnInit(): void {
    this.thingForm = this.formBuilder.group({
      desc: ['', [Validators.required] ]
    });
  }

}
