import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { FormBuilder, Validators } from '@angular/forms';

import { Paginated } from '@feathersjs/feathers';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService , Car } from '../services/data.service';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarsComponent implements OnInit {
  cars$: Observable<any[]>;
  carForm;
  
  constructor(
    private data: DataService,
    private formBuilder: FormBuilder
    ) {
      
      this.cars$ = data.cars$().pipe(
          map((t: Paginated<any>) => t.data)
        );
      
  }
    
  onSubmit(carData) {
    if(carData.make !== '' && carData.model !== '' && carData.year !== '' && carData.mileage !== '') {
      this.data.addCar( new Car(carData.make, carData.model, carData.year, carData.mileage) );
      
    }
  }
  
  onDelete(id) {
    this.data.deleteCar(id);
  }

  ngOnInit(): void {
    this.carForm = this.formBuilder.group({
      make: ['', [Validators.required, Validators.nullValidator] ],
      model: ['', [Validators.required, Validators.nullValidator] ],
      year: ['', [Validators.required, Validators.nullValidator] ],
      mileage: ['', [Validators.required, Validators.nullValidator] ]
    })
  }

}
