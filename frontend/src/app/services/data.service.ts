import { Injectable } from '@angular/core';
import { Feathers } from './feathers.service';

/**
 *  Abstraction layer for data management.
 */
@Injectable()
export class DataService {
  constructor(private feathers: Feathers) {
  }

  things$() {
    // just returning the observable will query the backend on every subscription
    // using some caching mechanism would be wise in more complex applications
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('things'))
      .watch()
      .find();
  }

  addThing(desc: string) {
    if (desc === '') {
      return;
    }

    // feathers-reactive Observables are hot by default,
    // so we don't need to subscribe to make create() happen.
    this.feathers
      .service('things')
      .create({
        desc
      });
  }
  
  cars$() {
    return (<any>this.feathers // todo: remove 'any' assertion when feathers-reactive typings are up-to-date with buzzard
      .service('cars'))
      .watch()
      .find();
  }
  
  addCar(car: Car) {
    if (!car) {
      return;
    }

    this.feathers
      .service('cars')
      .create({
        make: car.make,
        model: car.model,
        year: car.year,
        mileage: car.mileage
      });
  }
  
  getCars() {
    
    this.feathers
      .service('cars')
      .find({
        query: {
         
        }
      });
  }
  
  deleteCar(id: number) {
    this.feathers
      .service('cars')
      .remove(id)
  }
  
  
}

export class Car {
  make: string;
  model: string;
  year: number;
  mileage: number;
  
  constructor(make: string, model: string, year: number, mileage: number) {
      this.make = make;
      this.model = model;
      this.year = year;
      this.mileage = mileage;
  }
}