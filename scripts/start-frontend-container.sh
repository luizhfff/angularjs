#!/bin/sh
# THIS SCRIPT IS INTENDED TO BE RUN FROM THE auth1 PROJECT DIRECTORY
docker run --rm -it --network my-net --name frontend \
    -v "$PWD"/frontend:/mnt/frontend \
    -w /mnt/frontend     bkoehler/feathersjs sh
